/**
 * 
 * Classe de testes.
 * 
 * A correcao final irah considerar a execucao de todos os casos.
 * 
 */
public class Lista02Testes {

	public static void main(String[] args) {
		if (
				testesInserir() &
				testesElemento() &
				testesImprimir() &
				testesRemover() &
				testesPosicaoNome() &
				testesRecuperarIdades() &
				testesIdadesEmOrdemCrescente() &
				true
			) {
			System.out.println("Todos os testes executaram corretamente!");
		}
	}

	private static boolean testesInserir() {
		return 
		teste01() & 
		teste02() & 
		true;
	}

	private static boolean teste01() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		int tamanho = lista.getTamanho();
		if (tamanho != 3) {
			System.err.println("teste01 - 1");
			return false;
		}

		Pessoa d = new Pessoa("Pedro", 60);
		lista.inserir(d);

		tamanho = lista.getTamanho();
		if (tamanho != 3) {
			System.err.println("teste01 - 2");
			return false;
		}

		return true;
	}

	private static boolean teste02() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		Pessoa novaA = lista.getElemento(0);

		if (novaA == null) {
			System.err.println("teste02 - 1");
			return false;
		}

		if (!novaA.equals(a)) {
			System.err.println("teste02 - 2");
			return false;
		}

		int tamanho = lista.getTamanho();
		if (tamanho != 3) {
			System.err.println("teste02 - 3");
			return false;
		}

		return true;
	}

	private static boolean testesElemento() {
		return 
		teste03() & 
		teste04() & 
		true;
	}
	
	private static boolean teste03() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa nova = lista.getElemento(0);

		if (nova == null) {
			System.err.println("teste03 - 1");
			return false;
		}

		if (!nova.equals(c)) {
			System.err.println("teste03 - 2");
			return false;
		}

		return true;
	}

	private static boolean teste04() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(1);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa novaA = lista.getElemento(0);

		if (novaA == null) {
			System.err.println("teste04 - 1");
			return false;
		}

		if (!novaA.equals(a)) {
			System.err.println("teste04 - 2");
			return false;
		}

		Pessoa b = new Pessoa("Artur", 40);
		lista.inserir(b);

		Pessoa novaB = lista.getElemento(0);

		if (novaB == null) {
			System.err.println("teste04 - 3");
			return false;
		}

		if (!novaB.equals(a)) {
			System.err.println("teste04 - 4");
			return false;
		}

		return true;
	}

	private static boolean testesImprimir() {
		return 
		teste05() & 
		teste06() & 
		teste07() &
		true;
	}
	
	private static boolean teste05() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		String nomes = lista.imprimir();

		if (nomes == null) {
			System.err.println("teste05 - 1");
			return false;
		}

		if (!nomes.equals("{Joao, Marcelo, Maria}")) {
			System.err.println("teste05 - 2");
			return false;
		}

		return true;
	}

	private static boolean teste06() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(1);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa novaA = lista.getElemento(2);

		if (novaA != null) {
			System.err.println("teste06 - 1");
			return false;
		}

		String nomes = lista.imprimir();
		if (nomes == null) {
			System.err.println("teste06 - 2");
			return false;
		}

		if (!nomes.equals("{Joao}")) {
			System.err.println("teste06 - 3");
			return false;
		}

		return true;
	}

	private static boolean teste07() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Joao", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Joao", 50);
		lista.inserir(c);

		String nomes = lista.imprimir();

		if (nomes == null) {
			System.err.println("teste07 - 1");
			return false;
		}

		if (!nomes.equals("{Joao, Joao, Joao}")) {
			System.err.println("teste07 - 2");
			return false;
		}

		Pessoa novoB = lista.getElemento(1);
		if (novoB == null) {
			System.err.println("teste07 - 3");
			return false;
		}

		if (!novoB.equals(b)) {
			System.err.println("teste07 - 4");
			return false;
		}

		return true;
	}

	private static boolean testesRemover() {
		return 
		teste08() & 
		teste09() & 
		teste10() &
		teste11() &
		true;
	}
	
	private static boolean teste08() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		lista.remover(0);
		int tamanho = lista.getTamanho();

		if (tamanho != 2) {
			System.err.println("teste08 - 1");
			return false;
		}

		lista.remover(0);
		tamanho = lista.getTamanho();

		if (tamanho != 1) {
			System.err.println("teste08 - 2");
			return false;
		}

		lista.remover(0);
		tamanho = lista.getTamanho();

		if (tamanho != 0) {
			System.err.println("teste08 - 3");
			return false;
		}

		lista.remover(0);
		tamanho = lista.getTamanho();

		if (tamanho != 0) {
			System.err.println("teste08 - 4");
			return false;
		}

		return true;
	}

	private static boolean teste09() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		Pessoa novaC = lista.remover(2);

		if (novaC == null) {
			System.err.println("teste09 - 1");
			return false;
		}

		if (!novaC.equals(c)) {
			System.err.println("teste09 - 2");
			return false;
		}

		return true;
	}

	private static boolean teste10() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		Pessoa nova = lista.remover(5);

		if (nova != null) {
			System.err.println("teste10 - 1");
			return false;
		}

		int tamanho = lista.getTamanho();

		if (tamanho != 3) {
			System.err.println("teste10 - 2");
			return false;
		}

		return true;
	}

	private static boolean teste11() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		lista.remover(1);

		String nomes = lista.imprimir();

		if (nomes == null) {
			System.err.println("teste11 - 1");
			return false;
		}

		if (!nomes.equals("{Joao, Maria}")) {
			System.err.println("teste11 - 2");
			return false;
		}

		return true;
	}

	private static boolean testesPosicaoNome() {
		return 
		teste12() &
		teste13() &
		teste14() &
		teste15() &
		true;
	}

	private static boolean teste12() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		int posC = lista.posicaoNome("Maria");

		if (posC != 2) {
			System.err.println("teste12 - 1");
			return false;
		}

		int posNaoExiste = lista.posicaoNome("Pedro");

		if (posNaoExiste != -1) {
			System.err.println("teste12 - 2");
			return false;
		}

		return true;
	}

	private static boolean teste13() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(1);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		int posC = lista.posicaoNome("Joao");

		if (posC != 0) {
			System.err.println("teste13 - 1");
			return false;
		}

		return true;
	}

	private static boolean teste14() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(1);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		int posC = lista.posicaoNome("Maria");

		if (posC != -1) {
			System.err.println("teste14 - 1");
			return false;
		}

		return true;
	}

	private static boolean teste15() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		int posB = lista.posicaoNome("Marcelo");

		if (posB != 1) {
			System.err.println("teste15 - 1");
			return false;
		}

		int posA = lista.posicaoNome("Joao");

		if (posA != 0) {
			System.err.println("teste15 - 2");
			return false;
		}

		return true;
	}

	private static boolean testesRecuperarIdades() {
		return 
		teste16() &
		teste17() &
		teste18() &
		true;
	}
	
	private static boolean teste16() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		int[] idades = lista.recuperarIdades();

		if (idades == null) {
			System.err.println("teste16 - 1");
			return false;
		}

		if (idades.length != 3) {
			System.err.println("teste16 - 2");
			return false;
		}

		if (idades[0] != 40 || idades[1] != 31 || idades[2] != 50) {
			System.err.println("teste16 - 3");
			return false;
		}

		return true;
	}

	private static boolean teste17() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(1);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		int[] idades = lista.recuperarIdades();

		if (idades == null) {
			System.err.println("teste17 - 1");
			return false;
		}

		if (idades.length != 1) {
			System.err.println("teste17 - 2");
			return false;
		}

		if (idades[0] != 40) {
			System.err.println("teste17 - 3");
			return false;
		}

		return true;
	}

	private static boolean teste18() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		int[] idades = lista.recuperarIdades();

		if (idades != null) {
			System.err.println("teste18 - 1");
			return false;
		}

		return true;
	}
	
	private static boolean testesIdadesEmOrdemCrescente() {
		return 
		teste19() &
		teste20() &
		teste21() &
		teste22() &
		teste23() &
		true;
	}
	
	private static boolean teste19() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 31);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 50);
		lista.inserir(c);

		boolean isCrescente = lista.idadesEmOrdemCrescente();

		if (isCrescente == true) {
			System.err.println("teste19 - 1");
			return false;
		}

		return true;
	}

	private static boolean teste20() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 40);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 41);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 42);
		lista.inserir(c);

		boolean isCrescente = lista.idadesEmOrdemCrescente();

		if (isCrescente == false) {
			System.err.println("teste20 - 1");
			return false;
		}

		return true;
	}

	private static boolean teste21() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 42);
		lista.inserir(a);

		Pessoa b = new Pessoa("Marcelo", 41);
		lista.inserir(b);

		Pessoa c = new Pessoa("Maria", 40);
		lista.inserir(c);

		boolean isCrescente = lista.idadesEmOrdemCrescente();

		if (isCrescente == true) {
			System.err.println("teste21 - 1");
			return false;
		}

		return true;
	}
	
	private static boolean teste22() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		Pessoa a = new Pessoa("Joao", 42);
		lista.inserir(a);

		boolean isCrescente = lista.idadesEmOrdemCrescente();

		if (isCrescente == false) {
			System.err.println("teste22 - 1");
			return false;
		}

		return true;
	}
	
	private static boolean teste23() {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada(3);

		boolean isCrescente = lista.idadesEmOrdemCrescente();

		if (isCrescente == false) {
			System.err.println("teste23 - 1");
			return false;
		}

		return true;
	}

}
