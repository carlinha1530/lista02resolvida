/**
 * LISTA2 RESOLVIDA
 * Classe principal da lista. Todas as alteracoes para resolucao devem ser
 * colocadas aqui.
 * 
 * Esta implementacao deverah considerar uma lista estatica, usando vetores como
 * implementacao interna, sendo que os seus elementos devem ser mantidos
 * ordenados pelo campo nome da classe Pessoa (tambem em anexo).
 * 
 * DATA DE ENTREGA: 08/abr/2015 
 * 
 */
public class ListaEstaticaDesordenada {

	Pessoa[] dados;
	int qtdePessoas = 0;

	/**
	 * 
	 * Definicao do tamanho da lista
	 * 
	 * *** NAO ALTERAR ESTE METODO. ***
	 * 
	 * @param tamanhoLista
	 */
	public ListaEstaticaDesordenada(int tamanhoLista) {
		dados = new Pessoa[tamanhoLista];
	}

	/**
	 * 
	 * Este metodo deve tentar inserir um objeto do tipo Pessoa na proxima
	 * posicao livre da lista, ou seja, sempre no final. Caso tenha inserido
	 * corretamente, o metodo deve retornar true. Caso contrario, deve retornar
	 * false.
	 *  
	 * @param p
	 * @return
	 */
	public boolean inserir(Pessoa p) {
		if (qtdePessoas >= dados.length) {
			return false;
		}
		
		dados[qtdePessoas] = p;
		++qtdePessoas;
		
		return true;
	}

	/**
	 * 
	 * Este metodo deve retornar o numero de elementos existentes na lista neste
	 * momento.
	 * 
	 * @return
	 */
	public int getTamanho() {
		return qtdePessoas;
	}

	/**
	 * 
	 * Este metodo deve ser implementado de tal forma que ele retorne a Pessoa
	 * existente na posicao desejada, sendo que a primeira posicao deve comecar
	 * em zero. Caso seja informada uma posicao que nao esteja definida na
	 * lista ou um numero negativo, deve retornar null.
	 * 
	 * @param posicao
	 * @return
	 */
	public Pessoa getElemento(int posicao) {
		if (posicao > qtdePessoas) {
			return null;
		}
		
		return dados[posicao];
	}

	/**
	 * 
	 * O metodo imprimir deve retornar todos os nomes das pessoas existentes na
	 * lista, na ordem em que foram inseridos na lista. O formato deve seguir o
	 * padrao:
	 * 
	 * {Nome1, Nome2, ..., NomeN}
	 * 
	 * E caso a lista esteja vazia, deve retornar da seguinte forma: {}
	 * 
	 * @return
	 */
	public String imprimir() {
		String msg = "{";
		
		for (int i = 0; i < qtdePessoas; ++i) {
			msg += dados[i].nome + ", ";
		}
		
		if (qtdePessoas > 0) {
			msg = msg.substring(0, msg.length() - 2);
		}
		
		msg += "}";
		
		return msg;
	}

	/**
	 * 
	 * O metodo remover deve retornar o objeto Pessoa existente na posicao
	 * informada como parametro e retirar o objeto da lista. Caso a posicao
	 * desejada nao exista na lista, deve ser retornado null.
	 * 
	 * @param posicao
	 * @return
	 */
	public Pessoa remover(int posicao) {
		if (posicao > qtdePessoas || qtdePessoas == 0) {
			return null;
		}
		
		Pessoa temp = dados[posicao];
				
		for (int i = posicao; i < qtdePessoas - 1; ++i) {
			dados[i] = dados[i + 1];
		}
		
		--qtdePessoas;
		
		return temp;
	}

	/**
	 * 
	 * O metodo posicaoNome procura dentre os elementos da lista se existe algum
	 * com o nome informado como parametro e retorna a posicao deste elemento na
	 * lista. Caso nao encontre nenhum elemento com este nome, deve retornar o
	 * valor -1.
	 * 
	 * @param nomeProcurado
	 * @return
	 */
	public int posicaoNome(String nomeProcurado) {
		int posNomeAchado = -1;
		
		boolean achado = false;
		int i = 0;
		while (!achado && i < qtdePessoas) {
			if (dados[i].nome.equals(nomeProcurado)) {
				achado = true;
				posNomeAchado = i;
			}
			
			++i;
		}
		
		return posNomeAchado;
	}

	/**
	 * O metodo recuperarIdades deve percorrer todos os elementos da lista e 
	 * retornar as idades e somente as idades, de todas as pessoas que estiverem
	 * na lista. Caso a lista esteja vazia, deve retornar null.
	 * 
	 * @return
	 */
	public int[] recuperarIdades() {
		if (qtdePessoas == 0) {
			return null;
		}
		
		int[] idades = new int[qtdePessoas];
		
		for (int i = 0; i < qtdePessoas; ++i) {
			idades[i] = dados[i].idade;
		}
		
		return idades;
	}
	
	/**
	 * O metodo idadesEmOrdemCrescente deve verificar se todos os elementos da 
	 * lista estao em ordem crescente de idade, e em caso positivo, deve retornar 
	 * true. Se nao estiverem em ordem crescente, deve retornar false.
	 * 
	 * Adote como padrao que uma lista vazia estah sempre ordenada.
	 * 
	 * @return
	 */
	public boolean idadesEmOrdemCrescente() {
		boolean isCrescente = true;
		
		int i = 0;
		while (isCrescente && i < qtdePessoas - 1) {
			if (dados[i].idade > dados[i+1].idade) {
				isCrescente = false;
			}
			++i;
		}
		
		return isCrescente;
	}
}
